#!/bin/bash

###### Script to set up the Development Environment of the MicroPython GUI Project on the Lubuntu VM
echo
echo RUNNING THE SETUP OF THE ENVIRONMENT FOR THE MPYGUI DEVELOPMENT
echo THE SETUP MAY TAKE 30-60 MINUTES DEPENDING ON YOUR INTERNET CONNECTION
echo
echo INSTALLING 32-BIT LIBRARIES ...
echo
apt-get update
apt-get install lib32ncurses -y
apt-get install lib32z1 -y
apt-get install libc6:i386 libx11-6:i386 libxext6:i386 libstdc++6:i386 libexpat1:i386 -y


echo
echo STARTING DOWNLOAD OF PYCHARM IDE ...
echo
add-apt-repository ppa:mystic-mirage/pycharm -y
apt-get update
echo
echo STARTING INSTALLATION OF PYCHARM IDE ...
echo
sudo apt-get install pycharm-community -y


mkdir /home/mpygui/microchip_install_files
chown mpygui:mpygui /home/mpygui/microchip_install_files
cd /home/mpygui/microchip_install_files


echo
echo STARTING DOWNLOAD OF XC32 COMPILER ...
echo
wget www.microchip.com/mplabxc32linux
chmod +x mplabxc32linux
echo
echo STARTING INSTALLATION OF XC32 COMPILER ...
echo
/home/mpygui/microchip_install_files/mplabxc32linux -- --mode unattended --netservername ""
#Adding Path of compiler to PATH
export TMPVAR="$(find /opt/microchip/xc32/ -maxdepth 1 -type d -name 'v*')"
export TMPVAR="${TMPVAR}/bin"
export PATH=$PATH:$TMPVAR
echo "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:$TMPVAR" >> /home/mpygui/.profile 


echo
echo STARTING DOWNLOAD OF MPLABX IDE INSTALLER ...
echo
wget http://www.microchip.com/mplabx-ide-linux-installer
tar -xvf  mplabx-ide-linux-installer
rm mplabx-ide-linux-installer
echo
echo STARTING INSTALLATION OF MPLABX IDE ...
echo
find /home/mpygui/microchip_install_files/ -maxdepth 1 -type f -name 'MPLABX*'  -exec {} -- --mode unattended \;
find /home/mpygui/microchip_install_files/ -maxdepth 1 -type f -name 'MPLABX*' -exec rm {} \;


echo
echo STARTING DOWNLOAD OF MICROCHIP HARMONY FRAMEWORK ...
echo
wget http://ww1.microchip.com/downloads/en/DeviceDoc/harmony_v2_04_linux_installer.run
echo
echo STARTING INSTALLATION OF MICROCHIP HARMONY FRAMEWORK ...
echo
find /home/mpygui/microchip_install_files/ -maxdepth 1 -type f -name '*harmony*'  -exec chmod +x {} \;
find /home/mpygui/microchip_install_files/ -maxdepth 1 -type f -name '*harmony*'  -exec {} -- --mode unattended \;
find /home/mpygui/microchip_install_files/ -maxdepth 1 -type f -name '*harmony*' -exec rm {} \;
echo

echo
echo INSTALLING PYTHON PACKAGES...
echo
sleep 1
apt-get install python3-pip=8.1.1-2ubuntu0.4 -y
apt-get install python3-tk=3.5.1-1 -y
apt-get install python3-scipy=0.17.0-1 -y
apt-get install python3-matplotlib=1.5.1-1ubuntu1 -y

echo
echo SETTING UP DIRECTORIES...
echo
sleep 1
rm -rf /home/mpygui/microchip_install_files
mkdir /home/mpygui/PycharmProjects
chown -R mpygui:mpygui /home/mpygui/PycharmProjects
chown -R mpygui:mpygui /home/mpygui/microchip
ln -s /home/mpygui/microchip/harmony/v2_04/apps/ /home/mpygui/MPLABXProjects

echo
echo INSTALLING WINE...
echo
sleep 1
apt-get install wine=1:1.6.2-0ubuntu14.2 -y

echo
echo INSTALLING GIT...
echo
sleep 1
apt-get install git -y

echo "Now your environment is installed to work on the projects, please select if you want to fetch the projects"
echo
echo -n "Do you want to download the repositories for MPLABX (y/n)? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    #MPLABX
	cd /home/mpygui/MPLABXProjects
	git clone https://novotronik@bitbucket.org/mpy_gui/micropython_threading.git
	git clone https://novotronik@bitbucket.org/mpy_gui/micropython_gui.git
	chown -R mpygui:mpygui /home/mpygui/microchip
	chown -R mpygui:mpygui /home/mpygui/MPLABXProjects
fi

echo -n "Do you want to download the repositories for PyCharm (y/n)? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
	#PyCharm:
	cd /home/mpygui/PycharmProjects
	git clone https://novotronik@bitbucket.org/mpy_gui/mpy_module_generator.git
	git clone https://novotronik@bitbucket.org/mpy_gui/mpy_font_generator.git
	git clone https://novotronik@bitbucket.org/mpy_gui/demo_system.git
	git clone https://novotronik@bitbucket.org/mpy_gui/proof_of_concept.git
	git clone https://novotronik@bitbucket.org/mpy_gui/simulation_mvc.git
	chown -R mpygui:mpygui /home/mpygui/PycharmProjects
fi


echo THE MPLAB IDE AND PYCHARM IDE ARE NOW ACCESIBLE IN THE LAUNCHER IN THE PROGRAMMING TAB
echo PLEASE LOG OFF AND LOG IN TO APPLY CHANGES
echo
echo "Program terminated"