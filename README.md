
# Welcome to the micropython GUI Project.

This project offers you the posibility to program GUIs with the Power of Python (MicroPython) by using an economical embedded hardware with a Touchdisplay.


## 1 Download VM image
To get started, download the development environment VM image [here](https://mega.nz/#F!1FVD2SIQ!IlQsTfa4Sat66a5VS0cTew)


## 2 Download VirtualBox
To run the VM image you will need [VirtualBox](https://www.virtualbox.org/wiki/Downloads)


## 3 Start VM
With virtualbox, import the VM by double clicking on "mpygui_developer-release.vbox"

Then start the VM and log in:

	user:		mpygui
	password: 	mpygui


## 4 Auto-Install Environment on VM and Clone Repositories
When the VM is running, perform following steps:


### 4.1 IDEs, Compiler, Framework, Libraries
To install the IDEs, Compiler, Framework, Libraries etc. please open LXTerminal (on Desktop) and run 
	
	
	sudo /home/mpygui/install.sh 
	(password: mypgui)

The installation takes 30-60 Minutes (depending on your internet connection)


### 4.2 Clone repositories

At the  end of the installation you are prompted if you want to install the MPLABX- and Python-Projects.

To download the latest versions from this projects, type "y" and hit Enter.


#### 4.2.1 Pycharm Projects
The Python projects will be cloned into the directory "/home/mpygui/PycharmProjects"
The projects are:

		- demo_system.git
		- mpy_font_generator.git
		- mpy_module_generator.git
		- proof_of_concept.git
		- simulation_mvc.git
		
Then you can open the projects with Pycharm. Please set the interpreter to Python 3.5 at the first start of PyCharm


#### 4.2.2 MPLABX Projects
The C (MPLABX) Projects will to be cloned into the directory "/home/mpygui/MPLABXProjects"
The projects are:

		- micropython_gui.git
		- micropython_threading.git
		
Note, that "/home/mpygui/MPLABXProjects" is a symbolic link to "/home/mpygui/microchip/harmony/v2_04/apps"

To programm the Hardware, don't forget to pass the USB device to your Virtual Machine from your host system.


### 4.3 Log off and log in
After the installation, log off and log in again on the VM to apply changes.


## 5 Contact us
For further information about software and needed hardware contact us:
	
software@novotronik.com

[Novotronik Homepage](www.novotronik.com)
