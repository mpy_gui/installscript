#!/bin/bash
###### Script to download the installscript from Bitbucket and to execute it
###### EXECUTE SCRIPT WITH SUDO (Password: mpygui)
###### OPEN A TERMINAL AND PASTE FOLLOWING COMMAND:
###### sudo /home/mpygui/install.sh

if [ "$EUID" -ne 0 ]
  then echo "Please run the scipt as root (with sudo)"
  exit
fi

wget https://bitbucket.org/mpy_gui/installscript/raw/master/installscript.sh
chmod +x /home/mpygui/installscript.sh
/home/mpygui/installscript.sh